// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
// You code here ...
function Factory(ingredients, cost){
    return {
        ingredients,
        cost
    }
}
// Constructor Function
// You code here ...
function Product(ingredients, cost){
    this.ingredients = ingredients;
    this.cost = cost;
}

const popular = new Product(["лаваш мясной слаssис","картофел-фри"], 35000);
const retro= new Product(["шаурма мясной слаssис","картофел-фри"], 35000);
const traditional= new Product(["клаб-сендвич","картофел-фри"], 35000);
const trend = new Product(["гамбургер","картофел-фри"], 35000);

const factoryObject = new Factory("hotdog", 45000);
console.log(popular,retro,traditional,trend, factoryObject);